@extends('layout.master')
@section('title')
  Halaman Form
@endsection

@section('content')
    <form  action="/welcome" method="post">
      @csrf
      First Name :
      <br><br><input type="text" name="firstName" value=""><br><br>
    Last Name :
      <br><br><input type="text" name="lastName" value=""><br><br>
    Gender:
      <br><br><input type="radio" name="gender" value="">Male
      <br><input type="radio" name="gender" value="">Female
      <br><input type="radio" name="gender" value="">Other<br><br>
    Nationality:<br><br>
      <select name="nationality">
        <option value="">Indonesia</option><br><br>
        <option value="">Malaysia</option><br><br>
        <option value="">Singapura</option><br><br>
        <option value="">Thailand</option><br><br>
        <option value="">Vietnam</option><br><br>
        <option value="">Brunei</option><br><br>
      </select><br><br>
    Language Spoken:<br><br>
        <input type="checkbox" name="language" value="">Bahasa Indonesia <br>
        <input type="checkbox" name="language" value="">English <br>
        <input type="checkbox" name="language" value="">Other <br><br>
    Bio:<br><br>
        <textarea name="bio" rows="10" cols="30"></textarea><br>
      <input type="submit" name="" value="Sign Up">
    </form>
@endsection
