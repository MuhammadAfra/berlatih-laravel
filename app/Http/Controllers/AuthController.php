<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function daftar(){
      return view('page.form');
    }
    public function submit(Request $request){
      $first_name = $request['firstName'];
      $last_name = $request['lastName'];
      $gender = $request ['gender'];
      $national = $request ['nationality'];
      $spoken = $request ['language'];
      $bio = $request ['bio'];
      return view('page.welcome', compact('first_name','last_name','gender','national','spoken','bio'));
    }
}
